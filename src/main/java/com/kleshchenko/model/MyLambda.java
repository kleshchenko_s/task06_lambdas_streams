package com.kleshchenko.model;

@FunctionalInterface
public interface MyLambda {
    int run(int a, int b, int c);
}
