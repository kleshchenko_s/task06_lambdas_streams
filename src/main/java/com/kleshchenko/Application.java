package com.kleshchenko;

import com.kleshchenko.model.MyLambda;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Application {
    private static Logger log = LogManager.getLogger(Application.class);
    public static void main(String[] args) {
        MyLambda average = (a, b, c) -> (a+b+c)/3;
        MyLambda maximum = (a, b, c) -> {
            int d = a > b ? a : b;
            return d > c ? d : c;
        };
        log.info("average: " + average.run(20,10,30));
        log.info("\nmaximum: " + maximum.run(100,25,10));
    }
}
